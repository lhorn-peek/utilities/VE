﻿// ReSharper disable InconsistentNaming

using System;
using Authentication.Otp.Framework;
using Moq;
using NUnit.Framework;

namespace Authentication.Otp.Tests.OtpGenrator_Tests
{
    [Category("OtpGenerator")]
    public class When_generating_a_token : SpecificationBase
    {
        private OtpGenerator otpGenerator;
        private OtpToken otpToken;
        private Mock<IPasswordGenerator> _fakePasswordGenerator;
        private readonly DateTime _expires = DateTime.UtcNow.AddSeconds(30);
        private readonly string _key = Guid.NewGuid().ToString();
        private Mock<ISignatureProvider> _fakeSignatureProvider;

        protected override void Given()
        {

            _fakePasswordGenerator = new Mock<IPasswordGenerator>();
            _fakePasswordGenerator
                .Setup(p => p.GeneratePassword()).Returns("aPassword");

            _fakeSignatureProvider = new Mock<ISignatureProvider>();
            _fakeSignatureProvider
                .Setup(p => p.Sign(It.IsAny<SignatureSettings>())).Returns("hdnffgrtdggsbs64t46464ghe");

            otpGenerator = new OtpGenerator(_fakePasswordGenerator.Object, _fakeSignatureProvider.Object);
        }

        protected override void When()
        {
            otpToken = otpGenerator.NewToken(_key, _expires);
        }

        [Then]
        public void The_password_generator_was_called()
        {
            _fakePasswordGenerator.Verify(p => p.GeneratePassword());
        }

        [Then]
        public void The_signature_provider_was_called()
        {
            _fakeSignatureProvider.Verify(s => s.Sign(It.IsAny<SignatureSettings>()));
        }

        [Then]
        public void The_otpToken_should_NOT_be_null()
        {
            Assert.That(otpToken, Is.Not.Null);
        }

        [Then]
        public void The_otpToken_should_have_a_signature()
        {
            Assert.That(otpToken.Signature, Is.Not.Empty);
        }

        [Then]
        public void The_otpToken_expiry_should_NOT_have_changed()
        {
            Assert.That(otpToken.ExpiryDate, Is.EqualTo(_expires));
        }



    }
}
