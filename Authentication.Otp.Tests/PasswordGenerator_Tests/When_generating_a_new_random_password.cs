﻿// ReSharper disable InconsistentNaming

using Authentication.Otp.Framework;
using NUnit.Framework;

namespace Authentication.Otp.Tests.PasswordGenerator_Tests
{
    [Category("PasswordGenerator")]
    public class When_generating_a_new_random_password : SpecificationBase
    {
        private PasswordGenerator passwordGenerator;
        private const int PASSWORD_LENGTH = 10;
        private string password;

        protected override void Given()
        {
            passwordGenerator = new PasswordGenerator(PASSWORD_LENGTH);
        }

        protected override void When()
        {
            password = passwordGenerator.GeneratePassword();
        }

        [Then]
        public void The_password_should_NOT_be_null()
        {
            Assert.That(password, Is.Not.Null);
        }

        [Then]
        public void The_password_should_have_a_value()
        {
            Assert.That(password, Is.Not.Empty);
        }

        [Then]
        public void The_password_should_be_the_correct_length()
        {
            Assert.That(password.Length, Is.EqualTo(PASSWORD_LENGTH));
        }
    }
}
