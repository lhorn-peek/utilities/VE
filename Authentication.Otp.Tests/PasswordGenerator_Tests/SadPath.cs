﻿using System;
using Authentication.Otp.Framework;
using NUnit.Framework;

namespace Authentication.Otp.Tests.PasswordGenerator_Tests
{
    [Category("PasswordGenerator")]
    [TestFixture]
    public class SadPath
    {
        [Test]
        public void Should_FAIL_with_ArgumentException_when_password_length_is_zero()
        {
            const int INVALID_PASSWORD_LENGTH = 0;

            Assert.Throws<ArgumentException>(new TestDelegate(() => new PasswordGenerator(INVALID_PASSWORD_LENGTH)));

        }

        [Test]
        public void Should_FAIL_with_ArgumentException_when_password_length_is_LESS_than_zero()
        {
            const int INVALID_PASSWORD_LENGTH = -2;

            Assert.Throws<ArgumentException>(new TestDelegate(() => new PasswordGenerator(INVALID_PASSWORD_LENGTH)));

        }
    }
}
