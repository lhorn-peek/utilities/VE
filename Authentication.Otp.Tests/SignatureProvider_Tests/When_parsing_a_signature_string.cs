﻿// ReSharper disable InconsistentNaming

using System;
using Authentication.Otp.Framework;
using Moq;
using NUnit.Framework;

namespace Authentication.Otp.Tests.SignatureProvider_Tests
{
    [Category("SignatureProvider")]
    public class When_parsing_a_signature_string : SpecificationBase
    {
        private const string testSignatureString = "2y46dj576ythd";
        private Mock<ICryptor> fakeCryptor;

        private string expectedKey = "key";
        private string expectedPassword = "password";
        private DateTime expectedExpiry = DateTime.UtcNow;

        private string expectedDecryptedSignature;

        private SignatureState signatureState;
        private SignatureProvider signatureProvider;


        protected override void Given()
        {
            expectedDecryptedSignature = String.Format("{0},{1},{2}", expectedKey, expectedPassword, expectedExpiry.Ticks);
            fakeCryptor = new Mock<ICryptor>();
            fakeCryptor.Setup(h => h.Decrypt(It.IsAny<string>())).Returns(expectedDecryptedSignature);

            signatureProvider = new SignatureProvider(fakeCryptor.Object);
        }

        protected override void When()
        {
            signatureState = signatureProvider.Parse(testSignatureString);
        }

        [Then]
        public void The_cryptor_decrypt_function_is_called()
        {
            fakeCryptor.Verify(h => h.Decrypt(testSignatureString));
        }

        [Then]
        public void The_returned_signature_state_contains_the_correct_values()
        {
            Assert.AreEqual(expectedKey, signatureState.Key);

            Assert.AreEqual(expectedPassword, signatureState.Password);

            Assert.AreEqual(expectedExpiry, signatureState.ExpiryDate);
        }
    }
}
