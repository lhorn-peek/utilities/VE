﻿using Authentication.Otp.Framework;
using Moq;
using NUnit.Framework;

namespace Authentication.Otp.Tests.SignatureProvider_Tests
{
    [Category("SignatureProvider")]
    [TestFixture]
    public class Parsing_an_INVALID_signature_string
    {
        private string encryptedSignature;
        private string InvalidDecryptedSignature;
        private Mock<ICryptor> fakeCryptor;

        [SetUp]
        public void Setup()
        {
            encryptedSignature = "dnfhff65y5rnrff6";
            InvalidDecryptedSignature = "someInvalidString";

            fakeCryptor = new Mock<ICryptor>();
            fakeCryptor.Setup(c => c.Decrypt(encryptedSignature)).Returns(InvalidDecryptedSignature);
        }

        [Test]
        public void Should_throw_an_InvalidTokenSignature_Exception()
        {
            var signatureProvider = new SignatureProvider(fakeCryptor.Object);

            Assert.Throws<InValidTokenSignatureException>(new TestDelegate(() => signatureProvider.Parse(encryptedSignature)));
        }
    }
}
