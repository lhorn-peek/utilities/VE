﻿// ReSharper disable InconsistentNaming

using System;
using Authentication.Otp.Framework;
using Moq;
using NUnit.Framework;

namespace Authentication.Otp.Tests.SignatureProvider_Tests
{
    [Category("SignatureProvider")]
    public class When_creating_a_new_signature : SpecificationBase
    {
        private SignatureSettings settings;
        private SignatureProvider signatureProvider;
        private string signatureString;
        private readonly DateTime expiry = DateTime.UtcNow.AddSeconds(30);
        private Mock<ICryptor> fakeCryptor;
        private const string encryptedResult = "ghhgn5756ry5647";

        protected override void Given()
        {
            settings = new SignatureSettings("key", "password", expiry);

            fakeCryptor = new Mock<ICryptor>();
            fakeCryptor.Setup(h => h.Encrypt(It.IsAny<string>())).Returns(encryptedResult);

            signatureProvider = new SignatureProvider(fakeCryptor.Object);
        }

        protected override void When()
        {
            signatureString = signatureProvider.Sign(settings);
        }

        [Test]
        public void The_cryptor_is_called()
        {
            fakeCryptor.Verify(h => h.Encrypt(It.IsAny<string>()));
        }


    }
}
