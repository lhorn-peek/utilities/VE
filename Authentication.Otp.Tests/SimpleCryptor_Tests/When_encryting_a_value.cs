﻿// ReSharper disable InconsistentNaming

using Authentication.Otp.Framework;
using NUnit.Framework;

namespace Authentication.Otp.Tests.SimpleCryptor_Tests
{
    [Category("SimpleCryptor")]
    public class When_encryting_a_value : SpecificationBase
    {
        private const string decryptedValueExpected = "key,password,16353434475647";
        private SimpleCryptor cryptor;
        private string encryptedValue;
        private string decryptedValue;

        protected override void Given()
        {
            cryptor = new SimpleCryptor();
        }

        protected override void When()
        {
            encryptedValue = cryptor.Encrypt(decryptedValueExpected);
        }

        [Then]
        public void Decrypting_the_encrypted_value_should_return_the_correct_result()
        {
            decryptedValue = cryptor.Decrypt(encryptedValue);

            Assert.AreEqual(decryptedValueExpected, decryptedValue);
        }
    }
}
