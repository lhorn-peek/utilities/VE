﻿// ReSharper disable InconsistentNaming

using System;
using Authentication.Otp.Framework;
using NUnit.Framework;

namespace Authentication.Otp.Tests.OtpToken_Tests
{
    [Category("OtpToken")]
    [TestFixture]
    public class When_OtpToken_init 
    {
        private readonly string VALID_SIGNATURE = "36HDYTW44343GBBHE54";
        private readonly DateTime VALID_EXPIRY = DateTime.UtcNow.AddSeconds(30);
        private readonly string VALID_PASSWORD = "password";

        [Test]
        public void its_should_have_a_signature_when_init_VALID()
        {
            var sut = new OtpToken(VALID_SIGNATURE, VALID_EXPIRY, VALID_PASSWORD);

            Assert.That(sut.Signature, Is.Not.Empty);
        }

        [Test]
        public void it_should_throw_an_argument_exception_with_EMPTY_signature()
        {
            const string EMPTY_SIGNATURE = "";

            Assert.Throws<ArgumentException>(new TestDelegate(() => new OtpToken(EMPTY_SIGNATURE, VALID_EXPIRY, VALID_PASSWORD)));
        }

        [Test]
        public void it_should_throw_an_argument_exception_with_NULL_signature()
        {
            const string NULL_SIGNATURE = null;

            Assert.Throws<ArgumentException>(new TestDelegate(() => new OtpToken(NULL_SIGNATURE, VALID_EXPIRY, VALID_PASSWORD)));
        }


    }
}
