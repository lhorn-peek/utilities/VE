﻿// ReSharper disable InconsistentNaming

using System;
using Authentication.Otp.Framework;
using NUnit.Framework;

namespace Authentication.Otp.Tests.IntegrationTests
{
    //INTEGRATION TEST
 
    [Category("SignatureProvider_SimpleCryptor_Integration")]
    public class When_a_encrypted_signature_is_generated : SpecificationBase
    {
        private SignatureProvider signatureProvider;
        private ICryptor cryptor;
        private SignatureSettings signatureSettings;
        private readonly DateTime expiry = DateTime.UtcNow;
        private SignatureState signatureState;
        private SignatureState expectedState;
        private string encryptedSignature;

        protected override void Given()
        {
            cryptor = new SimpleCryptor();

            signatureProvider = new SignatureProvider(cryptor);
            signatureSettings = new SignatureSettings("aKey", "aPassword", expiry);
        }

        protected override void When()
        {
            encryptedSignature = signatureProvider.Sign(signatureSettings);
        }

        [Then]
        public void Decrypting_the_signatureString_returns_the_correct_state()
        {
            expectedState = new SignatureState("aKey", "aPassword", expiry);

            signatureState = signatureProvider.Parse(encryptedSignature);

            Assert.AreEqual(expectedState.Key, signatureState.Key);
            Assert.AreEqual(expectedState.Password, signatureState.Password);
            Assert.AreEqual(expectedState.ExpiryDate, signatureState.ExpiryDate);
        }
    }
}
