﻿// ReSharper disable InconsistentNaming

using System;
using Authentication.Otp.Framework;
using Moq;
using Ninject;
using NUnit.Framework;

namespace Authentication.Otp.Tests.IntegrationTests
{
    [Category("OtpGenerator_Integration")]
    public class When_getting_a_token_status_with_an_INVALID_token_signature : SpecificationBase
    {
        public When_getting_a_token_status_with_an_INVALID_token_signature()
        {
            kernel = new StandardKernel();

            kernel.Bind<IPasswordGenerator>().To<PasswordGenerator>().WithConstructorArgument("length", 5);
            kernel.Bind<ICryptor>().To<SimpleCryptor>();
            kernel.Bind<ISignatureProvider>().To<SignatureProvider>();
        }

        private StandardKernel kernel;

        private OtpGenerator otpGenerator;
        private Mock<IDateTimeWrapper> fakeDateTimeWrapper;
        private string INVALID_TOKEN_SIGNATURE;
        private OtpTokenStatus status;

        protected override void Given()
        {
            otpGenerator = kernel.Get<OtpGenerator>();

            fakeDateTimeWrapper = new Mock<IDateTimeWrapper>();
            fakeDateTimeWrapper.Setup(d => d.GetNow())
                .Returns(new DateTime()); 
        }

        protected override void When()
        {
            INVALID_TOKEN_SIGNATURE = "a";

            status = otpGenerator.GetTokenStatus("aKey", "aPassword", INVALID_TOKEN_SIGNATURE,
                fakeDateTimeWrapper.Object);
        }

        [Then]
        public void A_token_status_should_still_be_returned()
        {
            Assert.That(status, Is.Not.Null);
        }
    }
}
