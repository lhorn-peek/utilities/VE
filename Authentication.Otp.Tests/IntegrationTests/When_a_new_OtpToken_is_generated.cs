﻿// ReSharper disable InconsistentNaming

using System;
using Authentication.Otp.Framework;
using Moq;
using Ninject;
using NUnit.Framework;


namespace Authentication.Otp.Tests.IntegrationTests
{
    [Category("OtpGenerator_Integration")]
    public class When_a_new_OtpToken_is_generated : SpecificationBase
    {
        public When_a_new_OtpToken_is_generated()
        {
            kernel = new StandardKernel();

            kernel.Bind<IPasswordGenerator>().To<PasswordGenerator>().WithConstructorArgument("length", 5);
            kernel.Bind<ICryptor>().To<SimpleCryptor>();
            kernel.Bind<ISignatureProvider>().To<SignatureProvider>();
        }

        private StandardKernel kernel;
        private OtpGenerator otpGenerator;
        private OtpToken otpToken;
        private const string key = "1";
        private readonly DateTime expiry = DateTime.Now;
        private Mock<IDateTimeWrapper> fakeDateTimeWrapper;

        protected override void Given()
        {
            otpGenerator = kernel.Get<OtpGenerator>();

            fakeDateTimeWrapper = new Mock<IDateTimeWrapper>();

        }

        protected override void When()
        {
            otpToken = otpGenerator.NewToken(key, expiry);
        }

        [Then]
        public void The_token_should_have_state()
        {
            Assert.That(otpToken.Password, Is.Not.Empty);
            Assert.That(otpToken.Signature, Is.Not.Empty);
            Assert.That(otpToken.ExpiryDate, Is.EqualTo(expiry));
        }

        [Then]
        public void The_token_should_be_valid_within_30_seconds()
        {
            const double seconds = -30;

            fakeDateTimeWrapper.Setup(d => d.GetNow())
                .Returns(new DateTime(expiry.Ticks).AddSeconds(seconds));

            var status = otpGenerator.GetTokenStatus(key,
                                                     otpToken.Password,
                                                     otpToken.Signature,
                                                     fakeDateTimeWrapper.Object);

            Assert.IsFalse(status.HasExpired);

        }

        [Then]
        public void The_token_should_expire()
        {
            const double seconds = 1;

            fakeDateTimeWrapper.Setup(d => d.GetNow())
                .Returns(new DateTime(expiry.Ticks).AddSeconds(seconds));

            var status = otpGenerator.GetTokenStatus(key,
                                                     otpToken.Password,
                                                     otpToken.Signature,
                                                     fakeDateTimeWrapper.Object);

            Assert.True(status.HasExpired);
        }



    }
}
