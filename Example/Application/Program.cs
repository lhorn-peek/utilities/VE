﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Authentication.Otp.Provider;

namespace Application
{
    class Program
    {
        
        static void Main(string[] args)//
        {
            const double EXPIRE_SECONDS = 30;
            const int CURRENT_USER_ID = 112;
        
            Welcome();

            double amount = 0;
            while (amount <= 0)
            {
                Console.WriteLine("Enter the amount to be transfered?");
                Console.Write("£ ");
                var amountStr = Console.ReadLine();

                if (double.TryParse(amountStr, out amount) == false)
                {
                    Console.WriteLine("Invalid amount!");
                }
                Console.WriteLine("");
            }
            
            Console.WriteLine("Confirm and send OTP? (y / any key to exit)");
            string proceed = "";
            proceed = Console.ReadLine();

            if (string.IsNullOrEmpty(proceed) || proceed != "y")
            {
                Console.WriteLine("exiting...");
                Thread.Sleep(2000);
                Environment.Exit(0);
            }

            
            var otpProvider = new OneTimePasswordProvider();

            var token = otpProvider.CreateNewPasswordToken(CURRENT_USER_ID.ToString(), DateTime.Now.AddSeconds(EXPIRE_SECONDS));
            
            Console.WriteLine("OTP sent...");

            Console.WriteLine("Open your phone to view OTP? (y/n)");
            var openPhone = "";
            openPhone = Console.ReadLine();

            if (string.IsNullOrEmpty(openPhone) || openPhone != "y")
            {
                Console.WriteLine("Transfer cancelled");
                Console.WriteLine("exiting...");
                Thread.Sleep(2000);
                Environment.Exit(0);
            }

            ProcessStartInfo psi = new ProcessStartInfo("UserPhoneScreen.exe", token.Password);
            Process p = Process.Start(psi);


            var enteredPassword = "";
            while (String.IsNullOrEmpty(enteredPassword))
            {
                Console.WriteLine("Enter the sent OTP:");
                enteredPassword = Console.ReadLine();
            }

            p.Kill();

            var tokenStatus = otpProvider.VerifyPassword(CURRENT_USER_ID.ToString(), enteredPassword, token.Signature);

            if (tokenStatus.HasExpired)
            {
                Console.WriteLine("Sorry. Your OTP has either Expired, or the password was wrong.");
                Console.WriteLine("Goodbye!");
                Console.WriteLine("exiting...");
                Thread.Sleep(5000);
                Environment.Exit(0);
            }

            Console.WriteLine("Success! Your money has been transfered.");
            Console.WriteLine("press any key to exit...");

            Console.ReadLine();
        }

        static void Welcome()
        {
            Console.WriteLine("---------------------");
            Console.WriteLine("Money Transfer System");
            Console.WriteLine("---------------------");
            Console.WriteLine("\n");
            Console.WriteLine("Welcome Client. \n");

        }
    }
}
