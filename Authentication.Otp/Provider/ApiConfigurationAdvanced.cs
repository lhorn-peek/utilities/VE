﻿using System;
using Authentication.Otp.Framework;
using Ninject;

namespace Authentication.Otp.Provider
{
    internal class ApiConfigurationAdvanced
    {
        private StandardKernel _kernel;

        public OtpGenerator ConfigureApi()
        {
            _kernel = new StandardKernel();

            _kernel.Bind<IPasswordGenerator>().To<PasswordGenerator>().WithConstructorArgument("length", 5);
            _kernel.Bind<ICryptor>().To<SimpleCryptor>();
            _kernel.Bind<ISignatureProvider>().To<SignatureProvider>();
            _kernel.Bind<IDateTimeWrapper>().To<DateTimeWrapper>();

            return _kernel.Get<OtpGenerator>();
        }

        public T Resolve<T>()
        {
            try
            {
                var instance = _kernel.Get<T>();
                
                return instance;
            }
            catch (Exception e)
            {
                var msg = String.Format("cannot resolve type for: {0}.", typeof(T).FullName);

                var apiException = new ApiConfigException(msg, e);

                throw apiException;
            }
            
        }
    }
}
