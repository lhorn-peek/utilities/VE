﻿
using Authentication.Otp.Framework;

namespace Authentication.Otp.Provider
{
    internal class ApiConfiguration
    {
        public OtpGenerator ConfigureApi()
        {
            return new OtpGenerator(
                        new PasswordGenerator(5), 
                        new SignatureProvider(new SimpleCryptor()));
        }

        public IDateTimeWrapper GetInstance()
        {
            return new DateTimeWrapper();
        }
    }
}
