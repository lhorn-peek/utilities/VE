﻿using System;
using Authentication.Otp.Framework;

namespace Authentication.Otp.Provider
{
    public class OneTimePasswordProvider
    {
        #region config

        private ApiConfiguration _apiConfig;
        private OtpGenerator _otpGenerator;
        private IDateTimeWrapper _dateTimeWrapper;

        #endregion

        public OneTimePasswordProvider()
        {
            ConfigureApi();
        }

        public OtpToken CreateNewPasswordToken(string key, DateTime expires)
        {
            return _otpGenerator.NewToken(key, expires);
        }

        public OtpTokenStatus VerifyPassword(string key, string password, string tokenSignature)
        {
            return _otpGenerator.GetTokenStatus(key, password, tokenSignature, _dateTimeWrapper);
        }


        private void ConfigureApi()
        {
            _apiConfig = new ApiConfiguration();
            _otpGenerator = _apiConfig.ConfigureApi();
            _dateTimeWrapper = _apiConfig.GetInstance();
        }
    }
}
