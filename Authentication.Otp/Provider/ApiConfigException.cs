﻿
using System;

namespace Authentication.Otp.Provider
{
    public class ApiConfigException : Exception
    {
         public ApiConfigException()
        {
        }

        public ApiConfigException(string message)
            : base(message)
        {
        }

        public ApiConfigException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
