using System;

namespace Authentication.Otp.Framework
{
    public class SignatureState : SignatureSettings
    {
        public SignatureState(string key, string password, DateTime expiryDate) 
            : base(key, password, expiryDate)
        {
        }
    }
}