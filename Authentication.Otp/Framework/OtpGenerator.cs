﻿using System;

namespace Authentication.Otp.Framework
{
    public class OtpGenerator
    {
        private readonly IPasswordGenerator _passwordGenerator;
        private readonly ISignatureProvider _signatureProvider;

        public OtpGenerator(IPasswordGenerator passwordGenerator, ISignatureProvider signatureProvider)
        {
            _passwordGenerator = passwordGenerator;
            _signatureProvider = signatureProvider;
        }

        public OtpToken NewToken(string key, DateTime expires)
        {
            if (String.IsNullOrEmpty(key)) 
                throw new ArgumentException("Argument is null or empty", "key");

            var password = _passwordGenerator.GeneratePassword();

            var tokenSignature = _signatureProvider.Sign(new SignatureSettings(key, password, expires));

            return new OtpToken(tokenSignature, expires, password);
        }

        public OtpTokenStatus GetTokenStatus(string key, string password, string tokenSignature, IDateTimeWrapper dateWrapper)
        {
            #region validate args

            if (String.IsNullOrEmpty(key)) 
                throw new ArgumentException("Argument is null or empty", "key");

            if (String.IsNullOrEmpty(password)) 
                throw new ArgumentException("Argument is null or empty", "password");

            if (String.IsNullOrEmpty(tokenSignature))
                throw new ArgumentException("Argument is null or empty", "tokenSignature");

            #endregion

            try
            {
                var tokenState = _signatureProvider.Parse(tokenSignature);

                if ((tokenState.Key == key) & (tokenState.Password == password))
                    if(tokenState.ExpiryDate > dateWrapper.GetNow())
                        return new OtpTokenStatus(false, tokenSignature, tokenState.ExpiryDate);

                return new OtpTokenStatus(true, tokenSignature, tokenState.ExpiryDate);
            }
            catch (InValidTokenSignatureException)
            {
                return new OtpTokenStatus(true, tokenSignature, new DateTime());
            }

            
        }
    }
}
