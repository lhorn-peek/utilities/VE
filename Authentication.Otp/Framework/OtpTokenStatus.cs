﻿using System;

namespace Authentication.Otp.Framework
{
    public class OtpTokenStatus
    {
        private readonly bool _hasExpired;
        private readonly string _tokenSignature;
        private readonly DateTime _expiryDate;

        public OtpTokenStatus(bool hasExpired, string tokenSignature, DateTime expiryDate)
        {
            _hasExpired = hasExpired;
            _tokenSignature = tokenSignature;
            _expiryDate = expiryDate;
        }

        public bool HasExpired
        {
            get { return _hasExpired; }
        }

        public string TokenSignature
        {
            get { return _tokenSignature; }
        }

        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
        }
    }
}