﻿namespace Authentication.Otp.Framework
{
    public interface IPasswordGenerator
    {
        string GeneratePassword();
    }
}