namespace Authentication.Otp.Framework
{
    public interface ICryptor
    {
        string Encrypt(string value);

        string Decrypt(string hashedString);
    }
}