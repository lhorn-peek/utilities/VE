using System;

namespace Authentication.Otp.Framework
{
    public class SignatureSettings
    {
        private readonly string _key;
        private readonly string _password;
        private readonly DateTime _expiryDate;

        public SignatureSettings(string key, string password, DateTime expiryDate)
        {
            _key = key;
            _password = password;
            _expiryDate = expiryDate;
        }

        public string Key
        {
            get { return _key; }
        }

        public string Password
        {
            get { return _password; }
        }

        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
        }
    }
}