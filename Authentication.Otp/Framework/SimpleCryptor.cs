using System;
using System.Security.Cryptography;
using System.Text;

namespace Authentication.Otp.Framework
{
    public class SimpleCryptor : ICryptor
    {
        public string Encrypt(string value)
        {

            return Convert.ToBase64String(
                      ProtectedData.Protect(
                         Encoding.UTF8.GetBytes(value), null, DataProtectionScope.LocalMachine));
        }

        public string Decrypt(string hashedString)
        {
            return Encoding.UTF8.GetString(
                      ProtectedData.Unprotect(
                         Convert.FromBase64String(hashedString), null, DataProtectionScope.LocalMachine));
        }
    }
}