﻿using System;
using System.Text;

namespace Authentication.Otp.Framework
{
    public class PasswordGenerator : IPasswordGenerator
    {
        private int _length;

        public PasswordGenerator(int length)
        {
            if(length <= 0)
                throw new ArgumentException("must be greater than zero", "length");

            _length = length;
        }

        public string GeneratePassword()
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

            StringBuilder res = new StringBuilder();
            Random rnd = new Random();

            while (0 < _length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
    }
}