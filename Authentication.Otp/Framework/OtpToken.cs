using System;

namespace Authentication.Otp.Framework
{
    public class OtpToken
    {
        private readonly string _signature;
        private readonly DateTime _expiryDate;
        private readonly string _password;

        public OtpToken(string signature, DateTime expires, string password)
        {
            if (String.IsNullOrEmpty(signature))
                throw new ArgumentException("Argument is null or empty", "signature");

            if (expires == null)
                throw new ArgumentException("Argument cannot be less than or equal zero ", "expires");

            if (String.IsNullOrEmpty(password)) 
                throw new ArgumentException("Argument is null or empty", "password");

            _signature = signature;
            _expiryDate = expires;
            _password = password;
        }

        public string Signature
        {
            get { return _signature; }
        }

        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
        }

        public string Password
        {
            get { return _password; }
        }
    }
}