﻿using System;

namespace Authentication.Otp.Framework
{
    public class InValidTokenSignatureException : Exception
    {
        public InValidTokenSignatureException()
        {
        }

        public InValidTokenSignatureException(string message)
            : base(message)
        {
        }

        public InValidTokenSignatureException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
