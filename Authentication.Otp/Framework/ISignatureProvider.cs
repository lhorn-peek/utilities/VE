namespace Authentication.Otp.Framework
{
    public interface ISignatureProvider
    {
        string Sign(SignatureSettings signatureSettings);

        SignatureState Parse(string signature);
    }
}