﻿using System;

namespace Authentication.Otp.Framework
{
    public interface IDateTimeWrapper
    {
        DateTime GetNow();
    }
}