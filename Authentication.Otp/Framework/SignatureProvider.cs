using System;

namespace Authentication.Otp.Framework
{
    public class SignatureProvider : ISignatureProvider
    {
        private readonly ICryptor _cryptor;

        public SignatureProvider(ICryptor cryptor)
        {
            _cryptor = cryptor;
        }

        public string Sign(SignatureSettings signatureSettings)
        {
            var rawSignature = FormatRawSignature(signatureSettings);

            var hashedSignature = _cryptor.Encrypt(rawSignature);

            return hashedSignature;
        }


        public SignatureState Parse(string signature)
        {
            try
            {
                var rawSignature = _cryptor.Decrypt(signature);
                var signatureState = BuildState(rawSignature);

                return signatureState;
            }
            catch (Exception)
            {
                var exception = new InValidTokenSignatureException("tokenSignature is invalid.");
                throw exception;
            }

        }

        //can refactor the below private methods and move to another class,eg. ISettingsFormatter
        //(SRP), but will leave it for simplicity
        private string FormatRawSignature(SignatureSettings signatureSettings)
        {
            var rawSignature = String.Format("{0},{1},{2}",
                signatureSettings.Key,
                signatureSettings.Password,
                signatureSettings.ExpiryDate.Ticks);
            return rawSignature;
        }

        private SignatureState BuildState(string rawSignature)
        {

            var stateValues = rawSignature.Split(',');
            var ticks = Int64.Parse(stateValues[2]);

            var key = stateValues[0];
            var password = stateValues[1];
            var expiry = new DateTime(ticks);

            var state = new SignatureState(key, password, expiry);
            return state;

        }


    }
}